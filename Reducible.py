#  File: Reducible.py

#  Description: Finds reducible words within a dictionary

#  Student Name: Christopher Calizzi

#  Student UT EID:csc3322

#  Course Name: CS 313E

#  Unique Number: 50295

#  Date Created:4-2-20

#  Date Last Modified:4-2-20

# takes as input a positive integer n
# returns True if n is prime and False otherwise
def is_prime ( n ):
  if (n == 1):
    return False
  limit = int (n ** 0.5) + 1
  div = 2
  while (div < limit):
    if (n % div == 0):
      return False
    div += 1
  return True

# takes as input a string in lower case and the size
# of the hash table and returns the index the string
# will hash into
def hash_word (s, size):
    hash_idx = 0
    for j in range (len(s)):
        letter = ord (s[j]) - 96
        hash_idx = (hash_idx * 26 + letter) % size
    return hash_idx


# takes as input a string in lower case and the constant
# for double hashing and returns the step size for that
# string
def step_size (s, const):
    hash_step = 0
    for j in range(len(s)):
        letter = ord(s[j]) - 96
        hash_step = (hash_step * 26 + letter) % const
    if hash_step == 0:
        hash_step = 1
    return hash_step

# takes as input a string and a hash table and enters
# the string in the hash table, it resolves collisions
# by double hashing
def insert_word (s, hash_table):
    index = hash_word(s,len(hash_table))
    i = 0
    step = step_size(s,len(hash_table)-1)
    while hash_table[index+step*i]:
        i+=1
        if index + step*i>=len(hash_table):
            index = index + step*i
            index-= len(hash_table)
            i = 0
    hash_table[index+step*i] = s

# takes as input a string and a hash table and returns True
# if the string is in the hash table and False otherwise
def find_word (s, hash_table):
    index = hash_word(s, len(hash_table))
    i = 0
    step = step_size(s, len(hash_table)-1)
    while hash_table[index + step * i] and hash_table[index + step * i]!=s:
        i += 1
        #print("index",index)
        if index + step * i >= len(hash_table):
            index = index + step * i
            index -= len(hash_table)
            i = 0
            #print("Finding", "index: ", index, "i:", i)

    return hash_table[index + step*i] == s

# recursively finds if a word is reducible, if the word is
# reducible it enters it into the hash memo and returns True
# and False otherwise
def is_reducible (s, hash_table, hash_memo):
     if 'a' in s or 'i' in s or 'o' in s:
        if len(s) == 1:
            return True
        elif find_word(s,hash_memo):
            return True
        else:
            if find_word(s, hash_table):
                reducible = False
                i = 0
                while (not reducible) and i<len(s):
                    reducible = is_reducible(s[0:i]+s[i+1:len(s)],hash_table,hash_memo)
                    i +=1
                return reducible





# goes through a list of words and returns a list of words
# that have the maximum length
def get_longest_words (string_list):
    max = 0
    long_words = []
    for s in string_list:
        if s and len(s)>max:
            max = len(s)
            long_words = [s]
        elif s and len(s)==max:
            long_words.append(s)
    return long_words

def main():
  # create an empty word_list
  word_list = []
  # open the file words.txt
  f = open("words.txt","r")
  # read words from words.txt and append to word_list
  line = f.readline()
  while line:
      word_list.append(line[0:len(line)-1])
      line = f.readline()
  # close file words.txt
  f.close()
  # find length of word_list
  length = len(word_list)
  # determine prime number N that is greater than twice
  N = length*2 + 1
  while not is_prime(N):
      N+=1
  # create an empty hash_list
  hash_list = [None] * N
  # hash each word in word_list into hash_list
  # for collisions use double hashing
  for w in word_list:
      insert_word(w,hash_list)
  # create an empty hash_memo of size M
  # we do not know a priori how many words will be reducible
  # let us assume it is 10 percent (fairly safe) of the words
  # then M is a prime number that is slightly greater than
  # 0.2 * size of word_list
  M = length//5
  while not is_prime(M):
      M += 1
  # populate the hash_memo with M blank strings
  hash_memo = M * [None]
  # create an empty list reducible_words
  reducible_words = []
  # for each word in the word_list recursively determine
  # if it is reducible, if it is, add it to reducible_words
  for w in word_list:
      reducible = is_reducible(w,hash_list,hash_memo)
      if reducible:
          insert_word(w,hash_memo)
          reducible_words.append(w)
  # find words of the maximum length in reducible_words
  longest = get_longest_words(reducible_words)
  # print the words of maximum length in alphabetical order
  # one word per line
  longest.sort()
  print(longest)
# This line above main is for grading purposes. It will not
# affect how your code will run while you develop and test it.
# DO NOT REMOVE THE LINE ABOVE MAIN
if __name__ == "__main__":
  main()